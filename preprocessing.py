import numpy as np
from pathlib import Path
from scipy.signal import savgol_filter
import pandas as pd
import json

def consecutive(data, stepsize=1):
    """
    useful helper function to quickly find indeces of consecutive
    numbers in a numpy array.

    Taken from:
    https://stackoverflow.com/questions/7352684/how-to-find-the-groups-of-consecutive-elements-in-a-numpy-array
    """
    return np.split(data, np.where(np.diff(data) != stepsize)[0]+1)

def filter_data(folder, window=2, polyorder=4):
    """
    This function uses a Savitzky-Golay filter to remove digital noise
    from X/Y coordinates.

    The window size and polyorder can be changed by the user. They are
    usually used with the standard values.
    Input: a folder, either as a string or as a pathlib.Path object

    :return: pandas DataFrame containing Frame, Time and all columns
    containing 'X' and 'Y' (such as 'X-Centroid') but filtered
    """

    folder_ok = False
    try:
        Path(folder)
        folder_ok = True
    except Exception as caught_error:
        print(caught_error)
        print('Please enter path as string or pathlib object')

    if folder_ok:

        try:

            # Since there's the suboptimal naming convention when
            # collecting a video of 'data.csv' to denote info about the
            # stimulus and when then doing analysis it's also called
            # 'data.csv' the loop below first collects all data.csv names
            # and selects the newest one (as analysis must have been done
            # after the data collection).
            files_of_interest = []
            for i_file in Path(folder).iterdir():
                if 'data.csv' in i_file.name:
                    files_of_interest.append(i_file.name)
                    # data_name = i
                if 'experiment_settings.json' in i_file.name:
                    with open(i_file, 'r') as file:
                        experiment_settings = json.load(file)
                        recording_fps = experiment_settings['Framerate']

            if len(files_of_interest) == 1:
                data_name = files_of_interest[0]
            else:
                files_of_interest.sort()
                data_name = files_of_interest[-1]
            data_csv = pd.read_csv(Path(folder, data_name), sep=',')

        except Exception as caught_error:
            print(caught_error)
            print('Unable to read data.csv file.\n'
                  'Please ensure correct folder has been called')

    if folder_ok:
        window_size = recording_fps * window
        if window_size % 2 == 0:
            window_size += 1  # window size must be odd number

        # Currently, PiVR usually has the array populated with 0. This
        # is obviously problematic when trying to filter data.
        # To get around this, drop those columns.
        # First, find where column 'Time' is zero. The first two frames
        # are always zero so ignore those and ONLY take the frames
        # containing '0' in the 'Time' column at the end of the experiment
        to_drop = consecutive(np.where(data_csv['Time [s]'] == 0)[0])[-1]
        # Set this to zero
        data_csv.drop(to_drop, inplace=True)

        # Create empty dataframe to be populated
        filtered_data = pd.DataFrame()

        # Start by putting the Frame and time into the new columns
        filtered_data['Frame'] = data_csv['Frame']
        filtered_data['Time [s]'] = data_csv['Time [s]']
        # then loop through all columns
        for i_column in data_csv.columns:
            if 'X' in i_column or 'Y' in i_column:
                filtered_data[i_column] = \
                    savgol_filter(x=data_csv[i_column],
                                  window_length=window_size,
                                  polyorder=polyorder)
            else:
                # This makes sure we get all other columns as well
                filtered_data[i_column] = data_csv[i_column]

    return(filtered_data)

def align_experiments_by_timestamps(data, recording_time, framerate,
                                    additionally_align1=None, additionally_align2=None,
                                    synthetic_timestamps=False):
    """
    After doing a time-dependent stimulus it is necessary to align the timestamps of the experiments to each other
    instead of only using the index as not all experiments have the exact same timestamps at a given index.

    The timestamp defines the stimulation in PiVR (after v1.7.0)

    This function takes either the original data.csv file or the preprocessed.filter modified data as a dictionary.

    It also needs the longest recording time of the experiments that need to be compared (only tested with equally long
    recordings so far, though).

    Finally, it needs the framerate. Only tested with identical framerates but it might work if the fastest framerate
    is provided.

    The function returns numpy arrays containing:
    1) aligned timestamps
    2) aligned X-Head positions
    3) aligned Y-Head positions
    4) aligned X-Midpoint positions
    5) aligned Y-Midpoint positions
    6) aligned X-Tail positions
    7) aligned Y-Tail positions
    8) aligned X-Centroid positions
    9) aligned Y-Centroid positions

    additionally_align is an optimal DataSeries that can be added to the
    function

    synthetic_timestamps should only be used if data had to be interpolated due to skipped frames. You likely
    don't need this.

    """
    if synthetic_timestamps:
        timestamps_to_use = 'Synthetic Timestamps'
    else:
        timestamps_to_use = 'Time [s]'

    # find length of experiment
    theoretical_timestamps = np.linspace(0, recording_time,
                                         int(recording_time * framerate))
    folder_ok = True

    if folder_ok:
        # Note, I added the fill(np.nan) as there are sometimes missing timestamps
        correctly_aligned_timestamps = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_timestamps.fill(np.nan)

        correctly_aligned_heads_x = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_heads_x.fill(np.nan)

        correctly_aligned_heads_y = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_heads_y.fill(np.nan)

        correctly_aligned_midpoints_x = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_midpoints_x.fill(np.nan)

        correctly_aligned_midpoints_y = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_midpoints_y.fill(np.nan)

        correctly_aligned_tails_x = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_tails_x.fill(np.nan)

        correctly_aligned_tails_y = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_tails_y.fill(np.nan)

        correctly_aligned_centroids_x = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_centroids_x.fill(np.nan)

        correctly_aligned_centroids_y = np.zeros(
            (len(data), theoretical_timestamps.shape[0]))
        correctly_aligned_centroids_y.fill(np.nan)

        if additionally_align1 is not None:
            correctly_aligned_additionally_return1 = np.zeros((len(data), theoretical_timestamps.shape[0]))
            correctly_aligned_additionally_return1.fill(np.nan)

        if additionally_align2 is not None:
            correctly_aligned_additionally_return2 = np.zeros((len(data), theoretical_timestamps.shape[0]))
            correctly_aligned_additionally_return2.fill(np.nan)

        for outer_i, current_data in enumerate(data):
            # Each experiment
            for inner_i in range(correctly_aligned_timestamps.shape[1]):
                # each timepoint
                # but only if not zero
                # if all_timestamps_np[outer_i, inner_i] > 0:
                try:  # can happen to be out of bounds
                    if data[current_data][timestamps_to_use].iloc[inner_i] > 0:
                        # Identify the index in the optimal timestamps array for future use
                        # current_index = np.searchsorted(theoretical_timestamps,all_timestamps_np[outer_i, inner_i])
                        current_index = np.searchsorted(
                            theoretical_timestamps,
                            data[current_data][timestamps_to_use].iloc[inner_i])
                        #print(current_index)

                        # Align timestamps
                        # correctly_aligned_timestamps[outer_i, current_index] = all_timestamps_np[outer_i, inner_i]
                        correctly_aligned_timestamps[
                            outer_i, current_index] = \
                        data[current_data][timestamps_to_use].iloc[inner_i]

                        correctly_aligned_heads_x[outer_i, current_index] = \
                        data[current_data]['X-Head'].iloc[inner_i]
                        correctly_aligned_heads_y[outer_i, current_index] = \
                        data[current_data]['Y-Head'].iloc[inner_i]

                        correctly_aligned_midpoints_x[
                            outer_i, current_index] = \
                        data[current_data]['X-Midpoint'].iloc[inner_i]
                        correctly_aligned_midpoints_y[
                            outer_i, current_index] = \
                        data[current_data]['Y-Midpoint'].iloc[inner_i]

                        correctly_aligned_tails_x[outer_i, current_index] = \
                            data[current_data]['X-Tail'].iloc[inner_i]
                        correctly_aligned_tails_y[outer_i, current_index] = \
                            data[current_data]['Y-Tail'].iloc[inner_i]

                        correctly_aligned_centroids_x[outer_i, current_index] = \
                            data[current_data]['X-Centroid'].iloc[inner_i]
                        correctly_aligned_centroids_y[outer_i, current_index] = \
                            data[current_data]['Y-Centroid'].iloc[inner_i]

                        if additionally_align1 is not None:
                            correctly_aligned_additionally_return1[outer_i, current_index] = \
                                additionally_align1[current_data].iloc[inner_i]
                            #additionally_align1[outer_i].iloc[inner_i]
                        if additionally_align2 is not None:
                            correctly_aligned_additionally_return2[outer_i, current_index] = \
                                additionally_align2[current_data].iloc[inner_i]
                                #additionally_align2[outer_i].iloc[inner_i]

                except IndexError as e:
                    # This loop always breaks because the experiment stops at least 2 frames before the desired end for
                    # unknown reasons
                    # print(e)
                    # print('at inner_i ' + repr(inner_i))
                    break
        if additionally_align1 is None and additionally_align2 is None:
            return (correctly_aligned_timestamps,
                    correctly_aligned_heads_x,
                    correctly_aligned_heads_y,
                    correctly_aligned_midpoints_x,
                    correctly_aligned_midpoints_y,
                    correctly_aligned_tails_x,
                    correctly_aligned_tails_y,
                    correctly_aligned_centroids_x,
                    correctly_aligned_centroids_y)
        elif additionally_align1 is not None and additionally_align2 is None:
            return (correctly_aligned_timestamps,
                    correctly_aligned_heads_x,
                    correctly_aligned_heads_y,
                    correctly_aligned_midpoints_x,
                    correctly_aligned_midpoints_y,
                    correctly_aligned_tails_x,
                    correctly_aligned_tails_y,
                    correctly_aligned_centroids_x,
                    correctly_aligned_centroids_y,
                    correctly_aligned_additionally_return1)
        elif additionally_align2 is not None and additionally_align1 is not None:
            return (correctly_aligned_timestamps,
                    correctly_aligned_heads_x,
                    correctly_aligned_heads_y,
                    correctly_aligned_midpoints_x,
                    correctly_aligned_midpoints_y,
                    correctly_aligned_tails_x,
                    correctly_aligned_tails_y,
                    correctly_aligned_centroids_x,
                    correctly_aligned_centroids_y,
                    correctly_aligned_additionally_return1,
                    correctly_aligned_additionally_return2)


