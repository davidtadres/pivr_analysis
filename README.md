# PiVR_analysis

This repository currently contains functions that can be used to 
analyze experiments conducted using PiVR (www.pivr.org).

At the moment, it is easiest to just copy whatever function
you need into your script and call it. 

To run the example jupyter notebook please:

1. Install Git https://git-scm.com/downloads 

2. Install Miniconda on your PC: https://conda.io/miniconda.html.

3. After installation, open the anaconda prompt under Windows or a regular terminal under Mac/Linux.   

4. Clone the repository by typing in the anaconda prompt/terminal:
   
   `git clone https://gitlab.com/davidtadres/pivr_analysis`

5. Create a new python environment: 
   
   `conda create --name PiVR_analysis_env -y`

6. Activate the environment: 

   Use the following command under Windows

   `activate PiVR_analysis_env`

   Use the following command under Mac/Linux

   `source activate PiVR_analysis_env`

7. Now you need to install the necessary packages in the environment.
   Copy the commands line by line to your anaconda prompt and hit 
   enter.
   
   `conda install pandas -y`
 
   `conda install matplotlib -y`

   `conda install scipy -y`

   `conda install -c anaconda jupyter -y`

8. Open jupyter notebook by typing:

    `jupyter notebook`
