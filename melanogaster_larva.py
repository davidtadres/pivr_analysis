import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path
from shapely import geometry

def calculate_speed(x_filt, y_filt, pixel_per_mm, framerate):
    """
    Calculate Speed of animal. Make sure to use filtered data, for
    example by using the :filter_data: function to smoothen the data.

    Adapted from motion.m from the SOS publication: https://doi.org/10.1371/journal.pone.0041642

    First, distance is calculated over the previous and the next frame.
    dx = x_i-1 - x_i+1
    dy = y_i-1 - y_i+1
    Eucledian distance is just:
    distance = sqrt(dx^2 + dy^2)
    Then, normalize from pixel to mm by dividing by the px/mm factor
    Next, this is a bit unusual as we are integrating over 3 points instead of 2:
    We have to use the symmetric derivative
    https://en.wikipedia.org/wiki/Symmetric_derivative everything needs to be divided
    by 2
    speed = distance / 2 / framerate
    """
    speed = np.zeros((x_filt.shape[0]))
    speed.fill(np.nan)
    for i_frame in range(2, x_filt.shape[0] - 1):
        dx = (x_filt[i_frame + 1]) - (x_filt[i_frame - 1])
        dy = (y_filt[i_frame + 1]) - (y_filt[i_frame - 1])
        distance = np.sqrt(dx ** 2 + dy ** 2)
        distance /= pixel_per_mm
        speed[i_frame] = distance / (2 / framerate)
    return (speed)


def animal_orientation(midpoint_x, midpoint_y,
                       tail_x, tail_y):
    """
    Animal orientation is expressed as theta (Gomez Marin et al., 2011).

    The tail->midpoint angle used to calculate the animal orientation.
    Theta is an angle (-pi to +pi).

    Original Matlab code from SOS (Gomez-Marin et al.,2012) below:
    %dx=motorData{larvaIdx}.midXY(:,1)-motorData{larvaIdx}.tailXY(:,1);
    %dy=motorData{larvaIdx}.midXY(:,2)-motorData{larvaIdx}.tailXY(:,2);
    %theta=zeros(1,maxFrame);
    %theta(goodFrames)=atan2(dy(goodFrames),dx(goodFrames));
    %motorData{larvaIdx}.bodyTheta=theta;

    First draft of python code below
    #dx = all_data[current_data]['X-Midpoint'] - all_data[current_data]['X-Tail']
    #dy = all_data[current_data]['Y-Midpoint'] - all_data[current_data]['Y-Tail']
    #body_theta = np.arctan2(dy, dx)

    """
    delta_x = midpoint_x - tail_x
    delta_y = midpoint_y - tail_y
    body_theta = np.arctan2(delta_y, delta_x)

    return (body_theta)

def rate_of_change_in_orientation(body_theta, framerate):
    """
    Rate of change in orientation is defined as body Omega in Gomez Marin et al., 2011.

    Calculate the symmetric derivative (i-1 until i+1) of the animal orientation (theta)
    which has been unwrapped (=discontinuities removed).

    Original Matlab code from SOS (Gomez-Marin et al.,2012) below:
    % Rate of change in orienation
    %bodyOmega=zeros(1,maxFrame);
    %goodOmegaFrames=goodFrames;
    %for i=2:maxFrame-1;
        %only define the derivative when there are three good frames in a row
    %    if length(intersect(i-1:i+1,goodFrames))~=3
    %        goodOmegaFrames=setdiff(goodOmegaFrames,i);
    %    else
    %        data=unwrap(motorData{larvaIdx}.bodyTheta(i-1:i+1));
            %the symmetric difference, in rad/s
    %        bodyOmega(i)=fs*(data(3)-data(1))/2;
    %    end
    %end
    %motorData{larvaIdx}.bodyOmega=bodyOmega;
    """
    # This seems to be similar to the speed calculation
    body_omega = np.zeros((body_theta.shape[0]))
    body_omega.fill(np.nan)

    # unwrap needs to be given an array without NaN!
    unwrapped_theta = np.zeros((body_omega.shape[0]))
    unwrapped_theta.fill(np.nan)
    # removes 'jumps' in body theta
    unwrapped_theta[np.where(~np.isnan(body_theta))[0]] = np.unwrap(body_theta[np.where(~np.isnan(body_theta))[0]])
    for i_frame in range(2, unwrapped_theta.shape[0] - 1):
        # This is a bit convoluted:
        # (fps * (theta[i-1] - theta[i+1])) / 2
        # fps because we want a rate of change in seconds
        # divide by two because symmetric difference (we take the difference between i-1 and i+1 instead of e.g. i-1 and i)
        body_omega[i_frame] = framerate * (unwrapped_theta[i_frame - 1] - unwrapped_theta[i_frame + 1]) / 2

    return (body_omega)

def head_angle(midpoint_x, midpoint_y,
               head_x, head_y,
               body_theta):
    """
    Calculate head angle as defined in Gomez-Marin et al., 2011

    Original Matlab code from SOS (Gomez-Marin et al.,2012) below:
    % Relative head bending angle.
    %dx=motorData{larvaIdx}.headXY(:,1)-motorData{larvaIdx}.midXY(:,1);
    %dy=motorData{larvaIdx}.headXY(:,2)-motorData{larvaIdx}.midXY(:,2);
    %theta=zeros(1,maxFrame);
    %theta(goodFrames)=atan2(dy(goodFrames),dx(goodFrames));
    %aat=unwrap(theta)-unwrap(motorData{larvaIdx}.bodyTheta);
    %motorData{larvaIdx}.headTheta=atan2(sin(aat),cos(aat));
    """

    delta_x = head_x - midpoint_x
    delta_y = head_y - midpoint_y
    head_midpoint_angle = np.arctan2(delta_x, delta_y)
    # unwrap needs to be given an array without NaN!
    unwrapped_head_midpoint_angle = np.zeros((head_midpoint_angle.shape[0]))
    unwrapped_head_midpoint_angle.fill(np.nan)
    unwrapped_head_midpoint_angle[np.where(~np.isnan(head_midpoint_angle))[0]] = np.unwrap(
        head_midpoint_angle[np.where(~np.isnan(head_midpoint_angle))[0]])
    # unwrap needs to be given an array without NaN!
    unwrapped_body_theta = np.zeros((body_theta.shape[0]))
    unwrapped_body_theta.fill(np.nan)
    unwrapped_body_theta[np.where(~np.isnan(body_theta))[0]] = np.unwrap(
        body_theta[np.where(~np.isnan(body_theta))[0]])

    head_angle = unwrapped_head_midpoint_angle - unwrapped_body_theta

    head_theta = np.arctan2(np.sin(head_angle), np.cos(head_angle))

    return (head_theta)

def rate_of_change_of_bending(head_theta, framerate):
    """
    As defined in Gomez-Marin et al., 2011

    Original Matlab code from SOS (Gomez-Marin et al.,2012) below:
    % Rate of change in bending
    %omega=zeros(1,size(data_array,1));
    %for i=2:size(data_array,1)-1;
    %only define the derivative when there are three good frames in a row
    %if length(intersect(i-1:i+1,goodFrames))==3
    %    data=unwrap(motorData{larvaIdx}.headTheta(i-1:i+1));
        %the symmetric difference, in rad/s
    %    omega(i)=fs*(data(3)-data(1))/2;
    %end
    %end
    %motorData{larvaIdx}.headOmega=omega;
    """

    head_omega = np.zeros((head_theta.shape[0]))
    head_omega.fill(np.nan)
    # Unwrap needs to be given an array without NaN
    unwrapped_head_theta = np.zeros((head_theta.shape[0]))
    unwrapped_head_theta.fill(np.nan)
    unwrapped_head_theta[np.where(~np.isnan(head_theta))[0]] = np.unwrap(head_theta[np.where(~np.isnan(head_theta))[0]])
    for i_frame in range(2, unwrapped_head_theta.shape[0] - 1):
        # This is a bit convoluted:
        # (fps * (theta[i-1] - theta[i+1])) / 2
        # fps because we want a rate of change in seconds
        # divide by two because symmetric difference (we take the difference between i-1 and i+1 instead of e.g. i-1 and i)
        head_omega[i_frame] = framerate * (
                    unwrapped_head_theta[i_frame - 1] -
                    unwrapped_head_theta[i_frame + 1]) / 2

    return (head_omega)


def identify_turns_RDP(centroids_x, centroids_y):
    """
    This turn detection algorithm is robust as it only depends on the
    centroid position which can be tracked relatively faithfully by PiVR.

    RDP was first used by Zainab Bansfield to identify turns.
    I removed the dependency on Geopandas and made other changes to
    fit my current needs, e.g. I removed the clockwise/counterclockwise
    part and just return the indeces of the turns.

    The parameter
    """

    # this loop takes care of NaN while creating tuples of
    # x/y coordinates which need to be fed into shapely.geometry.LineString
    trajectory_coords_list = []
    for i in range(centroids_x.shape[0]):
        if ~np.isnan(centroids_x[i]):
            trajectory_coords_list.append(
                (centroids_x[i], centroids_y[i]))

    # create the LineString object
    trajectory = geometry.LineString(trajectory_coords_list)
    # Here the trajectory gets simplified. This line essentially takes
    # care of turn detection.
    simplified_trajectory = trajectory.simplify(tolerance=5, preserve_topology=False)

    # The coordinates in the simplified_trajectory are the turns.
    # Next, need to find the timepoints since that information is
    # unfortunately lost when using simplify.
    turns = []
    try: # old Shapely version
        for current_x, current_y in np.array(simplified_trajectory):
            turns.append(np.where(centroids_x == current_x)[0][0])
    except TypeError: # iteration over a 0-d array
        for current_x, current_y in simplified_trajectory.coords:
            turns.append(np.where(centroids_x == current_x)[0][0])

    # Ignore first and last point - these are not turns but beginning and end coordinates
    del turns[0]
    del turns[-1]

    return (turns)