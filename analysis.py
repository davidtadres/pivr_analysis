import numpy as np

def calculate_pulse_triggered_speed(data, pulse_timepoint, framerate, time_before, time_after):
    """
    This function takes speed, for example calculated with the
    function :calculate_speed: and a pulse_index provided by the user
    and calculates the speed around the pulse.

    It returns a numpy array with shape (time_after + time_before * framerate, len(pulse_index)).
    Missing data is indicates as NaN in the array.
    """

    # Go from seconds to index
    time_before_index = framerate * time_before
    time_after_index = framerate * time_after


    # Create an empty array to fill
    speed_around_pulse = np.zeros((time_before_index + time_after_index, len(pulse_timepoint)))
    speed_around_pulse.fill(np.nan)

    for counter, current_pulse in enumerate(pulse_timepoint):
        speed_around_pulse[:, counter] = data[current_pulse*framerate - time_before_index:current_pulse*framerate + time_after_index]

    return (speed_around_pulse)

def calculate_absolute_pulse_triggered_speed(data, pulse_timepoint, framerate, time_before, time_after):
    """
    This function takes speed, for example calculated with the
    function :calculate_speed: and a pulse_index provided by the user
    and calculates the ABSOLUTE speed around the pulse.

    This function is very similar to :calculate_pulse_triggered_speed: with
    the difference that it returns the absolute value. Useful for example
    to look at head angle speed

    It returns a numpy array with shape (time_after + time_before * framerate, len(pulse_index)).
    Missing data is indicates as NaN in the array.
    """

    # Go from seconds to index
    time_before_index = framerate * time_before
    time_after_index = framerate * time_after


    # Create an empty array to fill
    speed_around_pulse = np.zeros((time_before_index + time_after_index, len(pulse_timepoint)))
    speed_around_pulse.fill(np.nan)

    for counter, current_pulse in enumerate(pulse_timepoint):
        speed_around_pulse[:, counter] = np.abs(data[current_pulse*framerate - time_before_index:current_pulse*framerate + time_after_index])

    return (speed_around_pulse)

def delta_speed(speed_around_pulse, framerate):
    """
    This function returns the median speed before and after a stimulus pulse.

    The input data must be centered on the pulse, for example by using the returned
    data from function :plot_speed_around_switch:.

    For now, before and after switch is fixed to 2 seconds. Might change in the future!
    """
    before_switch = 2  # second
    after_switch = 2  # second

    ##################################

    before_switch *= framerate
    after_switch *= framerate

    center = int(speed_around_pulse.shape[1] / 2)

    # The proper way to do statistics will be to treat each animal as one measurement (not the switch). This will
    # keep each datapoint independent to conduct t-tests and such.
    before_pulse_median = np.nanmedian(speed_around_pulse[:,int(center - before_switch):center, :], axis=(1, 2))
    after_pulse_median = np.nanmedian(speed_around_pulse[:,center:int(center + after_switch), :], axis=(1, 2))
    tail_speed_delta = after_pulse_median - before_pulse_median


    return (tail_speed_delta)
