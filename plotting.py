import numpy as np
import matplotlib.pyplot as plt
from pathlib import Path



def moving_average(data, window_size):
    return np.convolve(data, np.ones(window_size), 'same') / window_size
def plot_speed_around_switch(data, color, label, framerate,
                             time_before, time_after,
                             ylabel, savepath, savename,
                             xlim=None, ylim=None):
    """
    This function plots speed data (comes as a list) around a defined index.
    data, color and label are lists (and must have the same length!)

    The data should be coming from :calculate_pulse_triggered_speed:. The
    pulse_index is a user provided list.
    Framerate is just an int
    time_before and time_after are numbers in seconds around the pulse that
    will be shown (and returned!).
    ylabel is a string allowing the user to label the y axis
    savepath is a pathlib.Path object where the plot should be saved
    savename is a string defining the name of the figure
    ylim is a tuple giving the user more control over how the plot should look
    like.
    """
    ########################
    time_before_index = framerate * time_before
    time_after_index = framerate * time_after

    # add 2 second to the part that's plotted to avoid ugly edges in the plot
    # time_before and time_after will be used later with ax.set_lim(-time_before, time_after)
    #time_before_index += framerate * 2
    #time_after_index += framerate * 2

    # plotting the smoothened data
    fig = plt.figure(figsize=(6, 6))
    ax = fig.add_subplot(111)
    time = np.linspace(-time_before_index / framerate,
                       time_after_index / framerate,
                       time_before_index + time_after_index)

    #########################

    for i in range(len(data)):

        median_speed_around_switch_data = np.nanmedian(
            data[i], axis=(0, 2))
        # SEM is calculated only on n of experiment, not n of switch + n. Discuss with Matthieu
        #sem_speed_around_switch_data = np.nanstd(speed_around_switch_data,
        #                                          axis=(1, 2)) / \
        #                                speed_around_switch_data.shape[2]

        #std_speed_around_switch_data = np.nanstd(speed_around_switch_data,
        #                                          axis=(1, 2))

        filtered_speed_data = moving_average(
            median_speed_around_switch_data, window_size=framerate)

        ax.plot(time, filtered_speed_data, c=color[i], label=label[i])
        # ax.plot(time, filtered_speed_data1 - std_speed_around_switch_data1,
        #        c=color1, ls='--')
        # ax.plot(time, filtered_speed_data1 + std_speed_around_switch_data1,
        #        c=color1, ls='--')
        # Individual traces:
        # ax.plot(time, speed_around_switch_data1.reshape(speed_around_switch_data1.shape[0],-1),
        #        c=color1, alpha=0.01)

    current_ylim = ax.get_ylim()
    ax.plot([0, 0], [0, 2], c='k', ls='--', zorder=-1)
    ax.set_ylim(current_ylim)


    if ylim is not None:
        ax.set_ylim(ylim)

    ax.set_ylabel(ylabel)
    ax.set_xlabel('Time [s]')

    if xlim is not None:
        ax.set_xlim(xlim)
    else:
        ax.set_xlim(-time_before, time_after)
    fig.legend()

    fig.savefig(Path(savepath, savename))